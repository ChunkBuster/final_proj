SET FOREIGN_KEY_CHECKS = 0;
#Add races
TRUNCATE TABLE race;
INSERT INTO race (id, base_hp, base_mp, description)VALUES 
(0, 100, 400, "Angel-touched"),
(1, 200, 150, "Halfwind"),
(2, 100, 250, "Rabbitfolk"),
(3, 75, 800, "Mothkin"),
(4, 800, 0, "Titanborn"),
(5, 2000, 2000, "Dragon"), #NPC-only races
(6, 1, 2000, "Wisp"), 
(7, 500, 0, "Construct"),
(8, 500, 100, "Golem"),
(9, 200, 300, "Kelpie"),
(10, 300, 200, "Nightmare (lesser)"),
(11, 2000, 3000, "Nightmare (greater)"),
(12, 0, 15000, "Seraphim"),
(13, 0, 15000, "Throne"),
(14, 0, 15000, "Archangel");


#SELECT 
#    *
#FROM
#    race;

#Add classes
TRUNCATE TABLE class;
INSERT INTO class (id, hp_bonus, mp_bonus, description)VALUES 
(0, 0, 200, "Cloudweaver"),
(1, 400, 0, "Warden"),
(2, -50, 1000, "Shattercaster"),
(3, 100, 100, "Dragonbreaker"),
(4, 0, 0, "Pantheon Apostle"),
(5, 0, 400, "Material Purjurer"),
(6, 0, 0, "Commoner"), #NPC-only classes
(7, 0, 0, "Abomination");

#SELECT 
#    *
#FROM
#    class;

#Add spells
TRUNCATE TABLE spell;
INSERT INTO spell (id, mana_cost, effectfile, soundfile,description)VALUES 
#General spells
(0, 100, "sparks", "poof", "Prestidigation"),
(1, 200, "fireball", "boom","Fireball"),
#Apostle spells
(2, 25, "lightray", "judgement", "Lay bare"),#Lay bare can be learned by other classes
(3, 25, "lightray_big", "hammer", "Condemn"),
(4, 25, "lightray_big", "hammer", "Exile"),
(5, 25, "chain", "shackle", "Pillory"),
(6, -100, "lightray", "chorus", "Heal wounds of clean"),
(7, -200, "lightray", "chorus", "Fester wounds of unclean"),
#Shattercaster spells
(8, 100, "shatter", "world_break", "Minor Shatter"),#Minor shatter can be learned by other classes
(9, 500, "shatter", "world_break", "Shatter"),
(10, 2000, "shatter", "world_break_big", "Great Shatter"),
#Cloudweaver spells
(11, 100, "cloud", "whoosh", "Cloudsurf"),
(12, 500, "shatter", "world_break", "Precipitation barrier"),
(13, 500, "null", "null", "Mist Golem"),
#Material purjurer spells
(14, 500, "lens_in", "air_clap", "Reject minor object"),
(15, 1000, "lens_in", "air_clap", "Reject object"),
(16, 2000, "lens_in", "air_clap", "Reject major object"),
(17, 100, "lens_out", "air_woosh", "Give rise to food"),
(18, 100, "lens_out", "air_woosh", "Give rise to drink"),
(19, 200, "lens_out", "air_woosh", "Give rise to weapons"),
(20, 50000, "lens_in", "massive_air_clap", "Reject mortal");

#SELECT 
#    *
#FROM
#    spell;

#Associate spells with classes
#todo: Convert this into actually being based on names rather than IDs if u have the time 
TRUNCATE TABLE class_can_cast;
INSERT IGNORE INTO class_can_cast (spell_id, class_id)VALUES 
(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), #Prestidigation
(1, 0), (1, 2), (1, 3), (1, 4), (1, 5), #Fireball
(2, 0), (2, 2), (2, 3), (2, 5), #Lay bare
(8, 0), (8, 2), (8, 3), (8, 4), (8, 5), #Minor shatter
(2,4),(3,4),(4,4),(5,4),(6,4),(7,4), #Apostle spells
(9,2),(10,2),#Shattercaster spells
(11,0), (12,0),(13,0),#Cloudweaver spells
(14,5), (15,5), (16,5),(17,5),(18,5),(19,5),(20,5);

#SELECT 
#    c.description, s.description
#FROM
#    class c
#        JOIN
#    class_can_cast cc ON c.id = cc.class_id
#        JOIN
#    spell s ON s.id = cc.spell_id;

#Items 
TRUNCATE TABLE item;
INSERT INTO item (weight, value, rarity, description)VALUES 
#Rubbish
(10, 	1,		100, 	"Rock (Rubbish)"),
(5, 	1,		100, 	"Old boot (Rubbish)"),
(2, 	1,		100, 	"Thrall Skull (Rubbish)"),
(1, 	1,		100, 	"Button (Rubbish)"),
#Basic equipment
(4, 	50,		10, 	"Steel Dagger"),
(20, 	100, 	10, 	"Steel Sword"),
(4,		50, 	10, 	"Wooden Shield"),
#Coins
(0, 	1, 		1000,	"Copper Coin"),
(0, 	10, 	100,	"Silver Coin"),
(0, 	100,	10,		"Gold Coin"),
(0,		1000, 	2,		"Platinum Coin"),
(0, 	10000, 	1,		"Titanite Coin"),
#Legendary weapons??
(100, 		0, 		1,	"THUNDERFURY, BLESSED BLADE OF THE WINDSEEKER"),
(140000, 	1600, 	1,	"literally a 14 ton boulder embedded into a tree"),
(150, 		5000, 	1,	"False Idol of Shibbodeth"),
(450, 		5000, 	1,	"Unbreakable Shackle of Helholm"),
(150, 		5000, 	1,	"The key to the gate above all");

#SELECT 
#    *
#FROM
#    item;

#Locations
TRUNCATE TABLE continent;
INSERT INTO continent (continent_desc)VALUES 
("That Which is Beneath All"),
("The Forgotten Lands"),
("The Great Climb");

#INSERT INTO region(id, continent_id, region_desc) VALUES
#(0, 0, "West");

#INSERT INTO @regionTypes VALUES ("North")
#INSERT INTO @regionTypes VALUES ("West")
#INSERT INTO @regionTypes VALUES ("South")
#INSERT INTO @regionTypes VALUES ("East")
#INSERT INTO @regionTypes VALUES ("Upper")
#INSERT INTO @regionTypes VALUES ("Lower")
#INSERT INTO @regionTypes VALUES ("Inner")

#DECLARE @zoneTypes TABLE (Value NVARCHAR(50))
#INSERT INTO @zoneTypes VALUES ("farm")
#INSERT INTO @zoneTypes VALUES ("townry")
#INSERT INTO @zoneTypes VALUES ("lake")
#INSERT INTO @zoneTypes VALUES ("field")
#INSERT INTO @zoneTypes VALUES ("chapel")
#INSERT INTO @zoneTypes VALUES ("ruin")
SET FOREIGN_KEY_CHECKS = 1;