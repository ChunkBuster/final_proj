import random
import mysql.connector
import sys
from mysql.connector import Error
from random_word import RandomWords
from requests.api import get

r = RandomWords()

adj_array = ["Sleepy", "Big", "Loathesome", "Tired", "Blessed", "Obliterated", "Wartorn", "Calming", 
            "Awe-Inspiring", "Famous", "Disgusting", "Whimsical", "Mystical", "Shattered", "Cloudbespoke",
            "Infamous", "Strange", "Draconic", "Dead", "Fecund", "Barren", "Musical", "Wooded", "Sharp", "Slaughter"]

noun_array = ["Rock", "Tree", "Dragon", "Skull", "Horse", "Cow", "Ale", "Wine", "Al-jabad", "", "Fruit", "Book", 
              "Mug", "Step", "Iron", "Steel", "Fire", "Animals", "Gold", "Glass", "Cloud", "Fog"]

firstname_array = ["Abby", "Frodo", "Chad", "Virgo", "Pelotrus", "Arnicus", "Grizzle", "Gimli", "Sam", 
                   "Galladriel", "Gallantium", "Eppy", "Esky", "Marcus", "Toad", "Lute", "Bean", "Celeste", "Isabelle",
                   "Christopher", "Isabelle", "Michie", "Leeroy"]

lastname_array = ["Proudpaw", "Scorned", "Manyfeet", "Cudlitz", "Wilson", "Sharpe", "Fropkin", "Cragborn", "Pitborn",
                  "Goldshim", "Horkle", "Jenkins", "McMahon", "Bloodborn", "Cloudborn", "Netherspawn", "Voidcall"]

def rand_adj():
    return adj_array[random.randint(0, len(adj_array) - 1)]

def rand_noun():
    return noun_array[random.randint(0, len(noun_array) - 1)]

def rand_firstname():
    return firstname_array[random.randint(0, len(firstname_array) - 1)]

def rand_lastname():
    return lastname_array[random.randint(0, len(lastname_array) - 1)]

def create_connection(host_name, port_num, user_name, user_password):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password,
            port=port_num
        )
        print("Connection to MySQL DB successful")
        #connection.set_current_schema("heavensgate_server")
    except Error as e:
        print(f"The error '{e}' occurred")
    return connection

def get_query_as_array(sql_cmd):
    cursor = connection.cursor()
    cursor.execute("USE heavensgate_server")
    cursor.execute(sql_cmd)
    records = cursor.fetchall()
    returned = []
    for r in records:
        arr = []
        returned.append(arr)
        for v in r:
            arr.append(v)
    cursor.close()
    return returned

def get_query_as_1d_array(sql_cmd):
    arr = get_query_as_array(sql_cmd)
    for i in range(0, len(arr)):
        arr[i] = str(arr[i])
    return arr

def create_regions(cursor):
    region_add_sql = "INSERT INTO region (continent_id, region_desc) VALUES (%s, %s)"
    continents = get_query_as_array("SELECT c.id, c.continent_desc FROM continent c")
    region_types = ["North", "West", "Upper", "Lower", "Inner", "Outer", "Lost"]
    
    params = []
    for continent in continents:
        continent_id = continent[0]
        for region in region_types:
            regionname = f"{region} {continent[1]}"
            params.append((int(continent_id), regionname))
            
    cursor.executemany(region_add_sql, params) 
    #print(get_query_as_array("SELECT * FROM region"))
    
def create_zones(cursor, num_to_add):
    zone_add_sql = "INSERT INTO zone (region_id, zone_desc) VALUES (%s, %s)"
    regions = get_query_as_array("SELECT r.id, r.region_desc FROM region r")
    zone_types = ["Chapel", "Farm", "Townry", "Lake", "Field", "Ruin", "Portal"]
    
    zone_params = []
    for region in regions:
        region_id = region[0]
        for i in range(0, num_to_add):
            zone = zone_types[random.randint(0, len(zone_types) - 1)] #Get random zone type, adjective, noun  
            zonename = f"{rand_adj()} {rand_noun()} {zone}"
            zone_params.append((region_id, zonename))
    cursor.executemany(zone_add_sql, zone_params)
    
def create_npc_controllers(max_npcs):
    zones = get_query_as_array("SELECT z.id FROM zone z")
    npc_controller_add_sql = "INSERT INTO npc_controller (zone_id, max_children) VALUES (%s, %s)"
    npc_controller_params = []
    for z in zones:
        npc_controller_params.append((z[0], random.randint(0, max_npcs)))
    cursor.executemany(npc_controller_add_sql, npc_controller_params)
    

def generate_users(cursor, num_to_add):
    user_add_sql = "INSERT INTO user (id, username, password) VALUES (%s, %s, %s)"
    moderator_add_sql = "INSERT INTO moderator (employee_id, user_id, salary, is_admin, supervisor_id) VALUES (%s, %s, %s, %s, %s)"
    #timecard_add_sql = "INSERT INTO timecard (employee_id, hours_worked) VALUES (%s, %s)"
    #admin_add_sql = "INSERT INTO administrator (user_id) VALUES (%s)"
    
    newuser_params = []
    newmod_params = []
    newadmin_params = []
    #newtimecard_params = []
    
    cur_user_id = 0
    cur_emp_id = 0
    for index in range(0, num_to_add):
        user_id = cur_user_id
        emp_id = cur_emp_id
        
        username = f"{rand_firstname()}{rand_adj()}{rand_noun()}"
        password = f"{rand_noun()}{rand_noun()}{random.randint(0, 1000)}"
        
        newuser_params.append((user_id, username, password))
        
        is_mod = random.randint(0, 100) > 99 or index == 0 #First user is moderator
        if(is_mod):
            is_admin = random.randint(0, 100) > 80 or index == 0 #First user is also admin
            if(is_admin):
                newadmin_params.append((emp_id, user_id, 150000, True, None))#Add the admin immediately
            else:#user is a regular moderator
                supervisor = newadmin_params[random.randint(0, len(newadmin_params) - 1)][0]
                newmod_params.append((emp_id, user_id, 50000, False, supervisor))
            #Generate timecard for the moderator
            #newtimecard_params.append((emp_id, random.randint(0, 80)))
            cur_emp_id += 1
        cur_user_id += 1
        
    cursor.executemany(user_add_sql, newuser_params)
    cursor.executemany(moderator_add_sql, newadmin_params)
    cursor.executemany(moderator_add_sql, newmod_params)
    #cursor.executemany(timecard_add_sql, newtimecard_params)
    #print(get_query_as_array("SELECT * FROM user"))
    #print(get_query_as_array("SELECT u.username FROM user u JOIN moderator m ON u.id = m.user_id"))
    #print(get_query_as_array("SELECT * FROM administrator"))

 
def generate_characters(cursor):
    def gen_char(id, max_race_id, max_class_id):
        name = f"{rand_firstname()} {rand_lastname()}"
        level = random.randint(0, 100)
        race_id = random.randint(0, max_race_id)
        class_id = random.randint(0, max_class_id)
        hp = random.randint(0, 100) #If I have time, go back and make this based on char stats 
        mp = random.randint(0, 100) #This too
        zone = zones[random.randint(0, len(zones) - 1)][0]
        gold = random.randint(0, 100) * level
        return(id, name, level, hp, mp, race_id, class_id, zone, gold)
    
    cur_char_id = 0
    #For now we'll generate one character per user and the max number of characters per controller
    character_add_sql = """
    INSERT INTO game_character 
    (id, character_name, level, 
    hp, mp, race_id, class_id, 
    current_zone, gold) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    zones = get_query_as_array("SELECT z.id FROM zone z")
    newchar_params = []
    
    #Player-owned char info
    user_owns_char_add_sql = "INSERT INTO user_owns_character (user_id, game_character_id) VALUES (%s, %s)"
    users = get_query_as_array("SELECT u.id FROM user u") 
    new_user_owner_params = [] 
    for u in users:
        newchar_params.append(gen_char(cur_char_id, 4, 5))
        new_user_owner_params.append((u[0], cur_char_id))
        cur_char_id += 1
    #Add user characters
    cursor.executemany(character_add_sql, newchar_params)
    cursor.executemany(user_owns_char_add_sql, new_user_owner_params)
    
    newchar_params.clear() #The memory impact was becoming a bit much
    controller_owns_char_add_sql = "INSERT INTO controller_owns_character (npc_controller_id, game_character_id) VALUES (%s, %s)"
    controllers = get_query_as_array("SELECT c.id, c.max_children FROM npc_controller c");
    new_controller_owner_params = []
    for c in controllers:
        for i in range(0, c[1]): 
            newchar_params.append(gen_char(cur_char_id, 14, 7))
            new_controller_owner_params.append((c[0], cur_char_id))
            cur_char_id += 1
    #Add npc characters
    cursor.executemany(character_add_sql, newchar_params)
    cursor.executemany(controller_owns_char_add_sql, new_controller_owner_params)
    #print("\n".join(get_query_as_1d_array("""SELECT u.username, c.level, r.description FROM user u
    #                         JOIN user_owns_character o ON u.id = o.user_id
    #                         JOIN game_character c ON c.id = o.game_character_id
    #                         JOIN race r ON r.id = c.race_id
    #                         ORDER BY u.username""")))
    #print("\n".join(get_query_as_1d_array("""SELECT z.zone_desc, r.description, c.level FROM npc_controller cn
    #                         JOIN controller_owns_character o ON cn.id = o.npc_controller_id
    #                         JOIN game_character c ON c.id = o.game_character_id
    #                         JOIN race r ON r.id = c.race_id
    #                         JOIN zone z ON z.id = cn.zone_id
    #                         ORDER BY z.zone_desc
    #                         LIMIT 1000""")))
#def generate_random_blocks(cusror, num_blocks):
    
    
    
#Establish connection
connection = create_connection("ix-dev.cs.uoregon.edu", "3602", "icudlitz", "Goober123@")
cursor = connection.cursor()
try:
    cursor.execute("USE heavensgate_server")
    if False:
    #Clean old data out     
        cursor.execute("SET FOREIGN_KEY_CHECKS = 0")
        cursor.execute("TRUNCATE TABLE user")
        cursor.execute("TRUNCATE TABLE zone")
        cursor.execute("TRUNCATE TABLE region")
        cursor.execute("TRUNCATE TABLE user")
        cursor.execute("TRUNCATE TABLE moderator")
        cursor.execute("TRUNCATE TABLE game_character")
        cursor.execute("TRUNCATE TABLE npc_controller")
        cursor.execute("TRUNCATE TABLE controller_owns_character")
        cursor.execute("TRUNCATE TABLE user_owns_character")
        #cursor.execute("TRUNCATE TABLE administrator")
        cursor.execute("SET FOREIGN_KEY_CHECKS = 1")

        #World constructs
        create_regions(cursor)
        create_zones(cursor, 80)
        create_npc_controllers(60)
        #Generated data
        generate_users(cursor, 1000)
        generate_characters(cursor)
    print("\n".join(get_query_as_1d_array("""SELECT r.region_desc, COUNT(*) FROM npc_controller cn
                            JOIN controller_owns_character o ON cn.id = o.npc_controller_id
                            JOIN game_character c ON c.id = o.game_character_id
                            JOIN race ra ON ra.id = c.race_id
                            JOIN zone z ON z.id = cn.zone_id
                            JOIN region r ON r.id = z.region_id
                            JOIN continent co ON co.id = r.continent_id
                            #ORDER BY co.continent_desc DESC
                            GROUP BY r.region_desc
                            LIMIT 1000""")))
finally:
    cursor.close()
    connection.commit()
    connection.close() #close connection